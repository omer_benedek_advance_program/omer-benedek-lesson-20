﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Omer_Benedek_Lesson_20
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            NetworkStream clientStream = connectToServer();
            Thread recDataThread = new Thread(() => getData(clientStream));
            recDataThread.Start();

            generateCards();



        }

        private void generateCards()
        {
            Point nextLocation = new Point(10, this.Height-170);
            for (int i = 0; i<10; i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "card" + i + 1;
                currentPic.Image = global::Omer_Benedek_Lesson_20.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    
                };

                this.Controls.Add(currentPic);
                nextLocation.X += currentPic.Size.Width + 10;            }
        }

        private NetworkStream connectToServer()
        {
            while(true)
            {
                try
                {
                    TcpClient client = new TcpClient();
                    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                    client.Connect(serverEndPoint);
                    NetworkStream clientStream = client.GetStream();
                    return clientStream;
                }
                catch
                {
                    DialogResult res = MessageBox.Show("Can't Connect to Server", "Connetion Error", MessageBoxButtons.RetryCancel);
                    if(res == System.Windows.Forms.DialogResult.Cancel)
                    {
                        Environment.Exit(0);
                    }
                }
            }
        }

        private void getData(NetworkStream clientStream)
        {
            while (true)
            {
                try
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = clientStream.Read(buffer, 0, 4096);
                    string data = System.Text.Encoding.UTF8.GetString(buffer);
                    string newData = "";
                    for(int i = 0; i<bytesRead; i++)
                    {
                        newData += data[i];
                    }

                    if (newData == "0000")
                    {
                        Invoke((MethodInvoker)delegate { button1.Enabled = true; });
                    }
                }
                catch
                {

                }
            }
        }

        private void sendData(NetworkStream clientStream)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("Hello Server!");

            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) //red x is pressed
        {
            Environment.Exit(0);
        }
    }
}
